// AUTHORS : Tino Nummela, Juho Kangas
// REST API demo
// 02/2022

#include <iostream>
#include <stdlib.h>
#include <string>
#include <time.h>
#include "restclient-cpp/restclient.h"
#include <nlohmann/json.hpp>
using json = nlohmann::json;

int main() {
    // Generating random number and converting it to string
    srand(time(0));
    int random_num;
    random_num = rand() % 898 + 1;
    std::string num_string;
    num_string = std::to_string(random_num);

    std::cout << "Catching pokemon\n";
    // Getting random pokemon information from pokeapi
    RestClient::Response initial_poke = RestClient::get("https://pokeapi.co/api/v2/pokemon/"+ num_string);

    std::cout << "\n";
    // Printing code to see if valid response is recieved
    std::cout << "Response code: " << initial_poke.code << "\n";
    // parsing data and printing the relevant information
    auto json_body = json::parse(initial_poke.body);
    json_body["name"];
    json_body["sprites"]["front_default"];
    json_body["types"][0]["type"]["name"];
    if(json_body["types"][1]["type"]["name"] != nullptr)
    {
        json_body["types"][1]["type"]["name"];        
    }

    // Preparing data in correct format to send to server
    json pokemon;
    pokemon["name"] = json_body["name"];
    pokemon["sprites"] = json_body["sprites"]["front_default"];
    pokemon["type1"] = json_body["types"][0]["type"]["name"];
    pokemon["type2"] = json_body["types"][1]["type"]["name"];
    pokemon.dump();
    
    json data;
    data["data"] = pokemon.dump();
    data["meta"] = "pokemon";
    data.dump();

    // fetching all data of pokemon already on the server
    RestClient::Response query_poke = RestClient::get("localhost:5000/fetch/all");

    // parsing the fetched data
    auto fetch_body = json::parse(query_poke.body);
    int count = fetch_body.size();
    std::string pokemon_data;// String to hold the data later
    
    // Iterating through the recieved pokemon data
    for(int i = 0; i < count; i++)
    {
        // Converting data to string
        pokemon_data = fetch_body[i]["data"];
        // String back to json
        auto parsed_pokemon = json::parse(pokemon_data);
        // If pokemon exists already, program ends
        if(parsed_pokemon["name"] == pokemon["name"])
        {
            std::cout<<"Pokemon already found, not added."<<"\n";
            return 0;
        }
        
    }
    // send pokemon to server if it does not exist
    RestClient::Response p = RestClient::post("http:/localhost:5000/submit", "application/json", data.dump());
    std::cout << "Response code: " << p.code << "\n";

    return 0;
}