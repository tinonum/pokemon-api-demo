## NAME
Pokemon Catcher

## DESCRIPTION
Assignment 4: Iot Project 1: Pokebot

## INSTRUCTIONS
How to run this on your own pc?

Make sure you have installed following packages/tools:
1. g++ (version 11)
2. cmake
3. vcpkg
4. restclient-cpp
5. nlohmann-json
6. Generic Python Rest Server (GPRS) 

```sh
$ git init
```
```sh
$ git clone git@gitlab.com:jhkangas3/pokemon-catcher.git
```

Remember to install server somewhere before this and run it.

```sh
$ python3 src/main.py
```
```sh
$ mkdir build && cd build
```
```sh
$ cmake../
```
```sh
$ make
```
```sh
$ ./pokemoncatcher
```

## PROGRAMFAQ
Program gets random pokemon from pokeapi.co and saves its name, types and sprite. 

Program check if given pokemon is already 'caught'. If it is, program won't post it to database.

## MAINTEINERS
Tino Nummela | [@tinonum](https://gitlab.com/tinonum)

Juho Kangas | [@jhkangas3](https://gitlab.com/jhkangas3)

**************************